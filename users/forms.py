from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from main.models import Person, Phone


class RegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=255, required=False)
    second_name = forms.CharField(max_length=255, required=False)
    email = forms.EmailField(max_length=255, required=False)

    def save(self, *args, **kwargs):
        data = self.cleaned_data
        django_profile = User.objects.create_user(
            username=data['username'],
            password=data['password2'],
            first_name=data['first_name'],
            last_name=data['second_name'],
            email=data['email']
        )
        Person.objects.create(user=django_profile)
        return django_profile


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ['phone_number', 'date_birth']


class PhoneForm(forms.ModelForm):
    class Meta:
        model = Phone
        # fields = '__all__'
        exclude = ['person']
