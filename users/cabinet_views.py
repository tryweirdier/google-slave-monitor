from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import PersonForm, PhoneForm
from main.models import Person, Phone
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


def settings(request):
    if request.user.is_authenticated:
        person = Person.objects.get(user=request.user)
        print(person)
        ctx = {
            'form': PersonForm(instance=person),
            'tab': 'settings'
        }
        if request.method == 'POST':
            person_form = PersonForm(instance=person, data=request.POST)
            first_name = request.POST.get('first_name')
            last_name = request.POST.get('last_name')
            email = request.POST.get('email')
            django_profile = User.objects.get(id=request.user.id)

            django_profile.first_name = first_name
            django_profile.last_name = last_name
            django_profile.email = email

            django_profile.save()
            if person_form.is_valid():
                person_form.save()
                return redirect('/users/cabinet/')
        return render(request, 'users/cabinet/main.html', ctx)
    else:
        return redirect('/users/login/')


def products(request):
    if request.user.is_authenticated:
        ctx = {
            'tab': 'products'
        }
        person = Person.objects.get(user=request.user)
        all_products = Phone.objects.filter(person=person)
        ctx['all_products'] = all_products
        return render(request, 'users/cabinet/products.html', ctx)
    else:
        return redirect('/users/login/')


def add_phone(request):
    if request.user.is_authenticated:
        ctx = {
            'tab': 'products',
            'form': PhoneForm
        }

        if request.method == 'POST':
            phone_form = PhoneForm(request.POST)
            if phone_form.is_valid():
                person = Person.objects.get(user=request.user)
                # phone_form.save()
                cleaned_data = phone_form.cleaned_data
                cleaned_data.update(person=person)
                Phone.objects.create(**cleaned_data)
                print(cleaned_data)
                ctx['result'] = True
        return render(request, 'users/products/phone.html', ctx)


def delete_phone(request, phone_id):
    if request.user.is_authenticated:
        try:
            person = Person.objects.get(user=request.user)
            phone = Phone.objects.get(id=phone_id)
            if phone.person.id == person.id:
                phone.delete()
            else:
                return HttpResponse('Но-но так делать!')
        except Exception as e:
            return HttpResponse('Phone not found! Please go to back ... ')
        return redirect('/users/my-products/')
    else:
        return redirect('/users/login/')
