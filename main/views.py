from django.shortcuts import render
from django.http import HttpResponse
from .models import Phone, Person


# Create your views here.


def home(request):
    all_phones = Phone.objects.all()
    total_sum = 0
    for phone in all_phones:
        total_sum += phone.get_price()
    return render(request, 'home.html', {
        'total_sum': total_sum,
        'all_phones': all_phones,
        'tab': 'home'
    })


def about(request):
    return render(request, 'about.html', {
        'a': 5 + 5,
        'list': [1, 2, 3, 4, 5],
        'tab': 'about'
    })


def phone_detail(request, phone_id):
    phone = Phone.objects.get(id=phone_id)
    print('phone_id  = ', phone)
    return render(request, 'phone_detail.html', {
        "phone": phone
    })


calc_map = {
    '+': lambda x, y: x + y,
    '-': lambda x, y: x - y,
    '*': lambda x, y: x * y,
    '/': lambda x, y: x / y,
}


def calculator(request):
    ctx = {
        'tab': 'calc',
        'digits': [i for i in range(10)]
    }
    if request.method == 'POST':
        try:
            first_number = float(request.POST.get('first_number'))  # ['first_number']
            operation = request.POST.get('operation')
            second_number = float(request.POST.get('second_number'))
            ctx['result'] = calc_map[operation](first_number, second_number)
        except Exception as e:
            ctx['error'] = str(e)
    return render(request, 'calculator.html', ctx)


def person_detail(request, person_id):
    ctx = {}
    ctx['person'] = Person.objects.get(id=person_id)
    return render(request, 'person/index.html', ctx)


def get_person_products(request, person_id):
    ctx = {}
    phones = Person.objects.get(id=person_id).get_phones()
    total_sum = sum([ph.get_price() for ph in phones])
    ctx['phones'] = phones
    ctx['person_id'] = person_id
    ctx['total_sum'] = total_sum
    return render(request, 'person/products.html', ctx)
def f():
    print('Hello world')