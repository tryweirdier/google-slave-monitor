from django.db import models
from django.contrib.auth.models import User

# Create your models here.
MAX_RATE = 5


class Person(models.Model):
    STATUS_CHOICES = (
        ('golden', 'Золотой продавец'),
        ('silver', 'Серебрянный продавец'),
        ('simple', 'Продавец'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=None)
    phone_number = models.CharField(max_length=20, blank=True, null=True)
    date_birth = models.DateField(blank=True, null=True)
    status = models.CharField(choices=STATUS_CHOICES, max_length=20)
    rate = models.FloatField(default=4, blank=True, null=True)
    count_sold_products = models.IntegerField(default=0)
    online = models.BooleanField(default=False)

    # TODO: add type of products

    def __str__(self):
        return '{} {} {}'.format(self.user.first_name, self.user.last_name, self.user.email)

    def get_name(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)

    def get_rate(self):
        return (self.rate / MAX_RATE) * 100

    def get_phones(self):
        return Phone.objects.filter(person=self)


class Phone(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, default=1, null=True, blank=True)
    name = models.CharField(max_length=50)
    brand = models.CharField(max_length=100)
    description = models.TextField(max_length=1000)
    price = models.FloatField(default=100)
    is_sale = models.BooleanField(default=False)
    percent_sale = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return '{} {} {}$'.format(self.name, self.brand, self.price)

    def get_price(self):
        if self.is_sale:
            return self.price * (1 - self.percent_sale / 100)
        else:
            return self.price
