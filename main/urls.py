from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.home),
    path('about/', views.about),
    path('calculator/', views.calculator),
    path('phone-detail/<int:phone_id>/' , views.phone_detail),
    path('get-person/<int:person_id>/' , views.person_detail),
    path('get-person-products/<int:person_id>/' , views.get_person_products)
]


def clown():
    print(clown)
